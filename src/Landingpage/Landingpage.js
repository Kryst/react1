import React from 'react';
import './Landingpage.css';

class Landingpage extends React.Component {
    constructor() {
        super();
    }
    render() {
        return    <section id="landingpage">
        <div className="landingpage-shadow">
          <div className="container landingpage-container">
            <div className="landingpage-text">
              <h1>Nasza firma oferuje najwyższej jakości usługi</h1>
              <h2>Sprawdź</h2>
              <a href="#offer"><button className="btn">oferta</button></a>
            </div>
          </div>
        </div>
      </section>
  
    }
    
}

export default Landingpage; 