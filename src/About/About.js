import React from 'react';
import './About.css';
import Person from './Person/Person.js';
import person1 from '../img/1.jpg'
import person2 from '../img/2.jpg'

class About extends React.Component {
    constructor(props) {
        super(props);

        this.persons = [
          {
              name: 'John',
              pic: person1
          },
          {
            name: 'Adrian',
            pic: person2
          },
          {
            name: 'Marek',
            pic: person1
          }
      ]
    }
    render() {
        return <section id="about">
    
            <div className="container about-container">
              
              <h1>Nasi specjaliści</h1>

                {this.persons.map((emp, i) => {
                  return <Person person={emp} key={i}></Person>
                })
              }
              
            </div>
        </section>
    }
}

export default About; 