import React from 'react';
import Nav from './Nav/Nav.js';
import Landingpage from './Landingpage/Landingpage.js'
import About from './About/About.js';
import Offer from './Offer/Offer.js';
import Footer from './Footer/Footer.js'
import './App.css';

function App() {
  return (
    <>
    <Nav></Nav>
    <Landingpage></Landingpage>
    <About></About>
    <Offer></Offer>
    <Footer></Footer>
    </>
  );
}

export default App;
