import React from 'react';
import Box from './Box/Box.js'
import './Offer.css';

class Offer extends React.Component {
    constructor(props) {
        super(props);


        this.offers = [
{
  name: 'Oferta1',
  isNew: true
},
{
  name: 'Oferta2',
  isNew: false
},
{
  name: 'Oferta3',
  isNew: true
},
{
  name: 'Oferta4',
  isNew: false
},
{
  name: 'Oferta5',
  isNew: true
},
{
  name: 'Oferta6',
  isNew: false
},

        ]
    }
    render() {
        return  <section id="offer">
        <div className="container">
          <h1>Czym zajmuje się nasza firma</h1>
    
          <div className="box-container">

            {
              this.offers.map((offer) => {
              return <Box box = {offer}></Box>
            })
            }
            
          </div>
        </div>
    </section>
        
    }
}

export default Offer; 