import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from "@fortawesome/free-solid-svg-icons";

import './Nav.css';

class Nav extends React.Component {
    constructor() {
        super();
    }
    render() {
        return <nav>
        <div className="container nav-container">
  
          <a href="#">Nazwa firmy</a>
  
          <div className="nav--links">
            <a href="#about">O nas</a>
            <a href="#offer">Oferta</a>
            <a href="#">Kontakt</a>
          </div>
  
          <div className="nav--toggler"><FontAwesomeIcon icon={faBars} /></div>
                      
        </div>
      </nav>  
  
    }
    
}

export default Nav; 