import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAtom } from "@fortawesome/free-solid-svg-icons";
import { faAward } from "@fortawesome/free-solid-svg-icons";
import './Footer.css';


class Footer extends React.Component {
    constructor() {
        super();
    }
    render() {
        return    <footer>
        <div className="container foot-section">
            <div className=" text-footer">
                Nazwa firmy - wszelkie prawa zastrzeżone, 2019.
            </div>
            <div className="icons">
                <FontAwesomeIcon icon={faAward} className="i"/>
                <FontAwesomeIcon icon={faAtom} />
            </div>
        </div>
      </footer>
  
  
    }
}

export default Footer; 